import RPi.GPIO as GPIO
import time

gpioBCMList = [2,3,4,17,27,22,10,9,11,5,6,13,19,26,14,15,18,23,24,25,8,7,12,16,20,21];
print("GPIO Pins: ", len(gpioBCMList) )
switchState = {};

# Configure GPIO and initialize switchState
GPIO.setmode(GPIO.BCM)
for port in gpioBCMList:
	GPIO.setup(port, GPIO.IN, pull_up_down = GPIO.PUD_UP)
	switchState[port] = 0

i = 30
while i > 0:
	for port in gpioBCMList:
		temp = GPIO.input( port )
		if temp != switchState[port]: print("Port ", port, " changed")
		switchState[port] = temp
	i -= 1
	time.sleep(1)

print("Finished")
GPIO.cleanup()
