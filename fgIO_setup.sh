#!/bin/bash 
# This script can be run on a fresh raspberry pi image to configure it as a Young Aviators IO device.
# run as root
# script needs root to edit root crontab so python script has GPIO access

echo "Configuring RaspberryPi for IO to FlightGear"

# Package Updates
sudo apt-get update

apt-get install -y git screen

# Clone git repos
cd /home/pi/
git clone https://YoungAviator@bitbucket.org/youngaviators/rpiscripts.git

# Cron configuration
(crontab -l; echo "@reboot sleep 60 && /usr/bin/screen -dmS fgIO python /home/pi/rpiscripts/flightgearlink.py") | crontab -

# Network configuration
echo "Configuring Network:"
 if !(grep "interface eth0" /etc/dhcpcd.conf >/dev/null 2>&1); then
echo "" >> /etc/dhcpcd.conf
echo "interface eth0" >> /etc/dhcpcd.conf
echo "" >> /etc/dhcpcd.conf
echo "static ip_address=192.168.0.206/24" >> /etc/dhcpcd.conf
echo "static routers=192.168.0.1" >> /etc/dhcpcd.conf
echo "static domain_name_servers=8.8.8.8" >> /etc/dhcpcd.conf
 else
echo "Network already configured"
 fi

# Reboot
reboot
