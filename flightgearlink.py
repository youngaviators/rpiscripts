# FlightGear Link Python Script
#	This script gathers data from the host Raspberry Pi's GPIO pins and sends it to
#	Flightgear in the proper format.

# 1. start thread to send data structure info to flightgear every X seconds
# 2. start thread to gather data from GPIO every Y seconds. update data structure
#		with info.

import platform
import os

b_RPi = platform.uname()[4].startswith("arm")

import threading
import socket
import time
import xml.etree.ElementTree as ET
if b_RPi: print("Running on RPi"); import RPi.GPIO as GPIO;

exitFlag = 0

#-----------------------------------------------
# Set up switchState Dictionary and msgFmt list
#-----------------------------------------------
switchState = {} # This lists all the items that are updated from the GPIO ports
msgFmt = [] # This lists the order and contents of the message sent to FlightGear
# [default value, GPIO pin] GPIP Pin of -1 causes update to be skipped

# parse the switchState and msgFmt information from the xml file
fn = os.path.join( os.path.dirname(os.path.abspath(__file__)) , 'SwitchIO.xml')
tree = ET.parse( fn )
root = tree.getroot()
for chunk in root.find('generic').find('input').iter('chunk'):
	#print( chunk.find('name').text )
	key = chunk.find('name').text
	try: bcmgpio = int( chunk.find('bcmgpio').text )
	except: bcmgpio = -1
	try: defaultval = int( chunk.find('defaultval').text )
	except: defaultval = 0
		
	switchState[key] = [defaultval, bcmgpio]
	msgFmt.append( key )

#---------------------------
# Define CmdListener Class
#---------------------------
class CmdListener (threading.Thread):
	def __init__(self, threadID, name):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		
		self.UDP_IP = '0.0.0.0'
		self.UDP_PORT = 5500
		self.server_address = (self.UDP_IP, self.UDP_PORT)
	
	def run(self):
		print( "Starting " + self.name )
		# define socket to listen for stop command
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.sock.bind( self.server_address )
		
		#self.sockLocal = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		#self.sockLocal.bind( ('localhost', self.UDP_PORT) )
		
		self.threadLoop();
		self.sock.close();
		print( "Exiting " + self.name )
		
	def threadLoop(self):
		global exitFlag
		while True:
			if exitFlag: break
			data, address = self.sock.recvfrom(4096)
			#dataLocal, address = self.sockLocal.recvfrom(4096)
			if data == b'STOP': exitFlag = 1
			#if dataLocal == b'STOP': exitFlag = 1
			time.sleep(1);

#-------------------------------------------------
# Define sendSwitch thread class
#-------------------------------------------------
class sendSwitch (threading.Thread):
	def __init__(self, threadID, name ):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		
		self.UDP_IP = '192.168.0.202'
		self.UDP_PORT = 5501
		self.server_address = (self.UDP_IP, self.UDP_PORT)
		
	def run(self):
		print( "Starting " + self.name )
		
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
		self.threadLoop()
		self.sock.close()
		print( "Exiting " + self.name )
		
	def threadLoop(self):
		while True:
			if exitFlag: break
			message = self.msgGen()
			self.sock.sendto(message, self.server_address)
			self.sock.sendto(message, ('localhost',self.UDP_PORT) )
			time.sleep(.1)
	
	def msgGen(self):
		message = ''
		for key in msgFmt:
			temp = switchState[key][0]
			if temp: temp = 1
			elif not temp: temp = 0
			#message += str(switchState[key][0])
			message += str(temp)
			message += ',' # value separator
		message = message[:-1]+'\n' # line separator
		message = message.encode('ascii')
		return message

#-------------------------------------------------
# Define GatherSwitch Class
#-------------------------------------------------
class getSwitch (threading.Thread):
	def __init__(self, threadID, name ):
		threading.Thread.__init__(self)
		self.threadID = threadID
		self.name = name
		
	def run(self):
		print( "Starting " + self.name )
		if b_RPi: self.configGPIO()
		self.threadLoop()
		if b_RPi: GPIO.cleanup()
		print( "Exiting " + self.name )

	def threadLoop(self):
		while True:
			if exitFlag: break
			
			#update the vars from GPIO here
			if b_RPi: self.updateFromGPIO()

			time.sleep(0.025)

	def configGPIO(self):
		GPIO.setmode(GPIO.BCM)
		for key in msgFmt:
			GPIO.setup(switchState[key][1], GPIO.IN, pull_up_down = GPIO.PUD_UP)
	
	def updateFromGPIO(self):
		for key in msgFmt:
			pin = switchState[key][1]
			if pin != -1: switchState[key][0] = not GPIO.input( pin )

#-------------------------------------------------
# Create new threads
#-------------------------------------------------
listener = CmdListener(1, "CmdListen-1")
sender = sendSwitch(2, "sendSwitch-1")
getter = getSwitch(3, "getSwitch-1")

# Start new Threads
listener.start()
sender.start()
getter.start()
