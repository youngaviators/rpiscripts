import socket

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

UDP_IP = '192.168.0.206'
UDP_PORT = 5500
server_address = ( UDP_IP, UDP_PORT )

sock.sendto( b'STOP', server_address )
sock.sendto( b'STOP', ('localhost', UDP_PORT) )